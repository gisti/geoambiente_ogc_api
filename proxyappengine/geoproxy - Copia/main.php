<?php 	

	header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1	
	$basefolder = "https://storage.googleapis.com/emplasa-webgis-tile-cache/cache/" . $_GET['folder'] . "/" . $_GET['z'] . "/" . $_GET['x'] . "/" . $_GET['y'];		
    
    if (getimagesize($basefolder . ".png") !== false) {
        $im = imagecreatefrompng($basefolder . ".png");  

		header('Content-Type: image/png');    
        readfile($basefolder . ".png");        
    }
    else if (getimagesize($basefolder . ".jpg") !== false) {
        $im = imagecreatefrompng($basefolder . ".jpg");  

		header('Content-Type: image/jpg');    
        readfile($basefolder . ".jpg");        
    }    
    else{
    	header('Content-Type: image/png');    
        readfile("https://storage.googleapis.com/emplasa-webgis-tile-cache/transp.png");	
    }
?>